#include <stdio.h>
#include <math.h>
#include <conio.h>
int main()
{
	//noktalarin degerlerini kullanicidan alma
	int x1, y1, x2, y2;
	printf("\nIKI NOKTA ARASI UZAKLIK HESAPLAMA\n\n");
	printf("(x1, y1) ve (x2, y2) noktalarin degerlerini gir\n");
	printf("x1: ");
	scanf("%d",&x1);
	printf("y1: ");
	scanf("%d",&y1);
	printf("x2: ");
	scanf("%d",&x2);
	printf("y2: ");
	scanf("%d",&y2);
	
	//iki nokta arasi uzaligi hesaplayip degiskene atama
	float uzaklik=sqrt(pow((x2-x1),2)+pow((y2-y1),2));
	
	//hesaplanan uzakligi ekrana yazdirma
	printf("\n(%d,%d) ve (%d,%d) noktalari arasindaki uzaklik: %f birim\n",x1,y1,x2,y2,uzaklik);

	getch();
	return 0;
}