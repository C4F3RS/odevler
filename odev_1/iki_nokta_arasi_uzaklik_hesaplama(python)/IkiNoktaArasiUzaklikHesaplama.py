print("\nİKİ NOKTA ARASI UZAKLIK HESAPLAMA\n")

#değerleri kullanıcıdan alma
print("(x1, y1) ve (x2, y2) noktaların değerlerini gir")
x1=int(input("x1: "))
y1=int(input("y1: "))
x2=int(input("x2: "))
y2=int(input("y2: "))

#uzaklığı hesaplayıp değişkene atama
#karekök almak için 1/2 üssünü aldım math kütüphanesini import edip sqrt() fonsiyonuda kullanılabilir
uzaklik=((x2-x1)**2+(y2-y1)**2)**(1/2)

#hesaplanan uzaklığı ekrana yazdırma
print("\n("+str(x1)+","+str(y1)+") ve ("+str(x2)+","+str(y2)+") noktaları arasındaki uzaklık:",uzaklik,"birim")