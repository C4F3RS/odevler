print("\nİKİ MATRİSİN ÇARPIMI\n")
matris1SatirSayisi = int(input("Birinci matrisin satır sayısını gir: "))

matris1 = []
matris2 = []

print("\nSatırdaki sayıları girerken aralarında boşuk bırak!\n")

#matrisleri kullanıcıdan alma
#kullanıcıdan alınan matrisin satırını split() ile boşluklardan ayırıp dizi haline getiriliyor
#map() fonsiyonuyla dizideki her bir elemanı intergera çevriliyor
#append() fonksiyonuyla dizinin içine bir dizi yani matrisin satır dizileri ekleyeneyek 2 boyutlu bir matris dizisi olmuş oluyor
for x in range(matris1SatirSayisi):
	matris1.append(list(map(int, input("Birinci matrisin "+str(x+1)+". satırını gir: ").split(" "))))
	#eğer matrisin ilk satırın sutun sayısı ile diğer satırların sutun sayıları aynı girilmezse program hata verip kapanacak
	if len(matris1[0]) != len(matris1[x]):
		print("\nHatalı giriş!")
		exit()
matris1SutunSayisi=len(matris1[0])
matris2SatirSayisi = matris1SutunSayisi #çarpım kuralı gereği ikinci matrisin satır sayısı ile birinci matrisin sutun sayısı aynı olmak zorunda
print("\nİkinci matrisin satır sayısı:",matris2SatirSayisi,"\n")
for y in range(matris2SatirSayisi):
	matris2.append(list(map(int, input("İkinci matrisin "+str(y+1)+". Satırını gir: ").split(" "))))
	if len(matris2[0]) != len(matris2[y]):
		print("\nHatalı giriş!")
		exit()

matris2SutunSayisi = len(matris2[0])

#matrislerin çarpımı
sonucMatris=[]
sonucMatrisSatir=[]
toplam=0
for a in range(matris1SatirSayisi):
		for b in range(matris2SutunSayisi):
			for c in range(matris1SutunSayisi):
				toplam+=matris1[a][c] * matris2[c][b]
			sonucMatrisSatir.append(toplam)
			toplam=0
		sonucMatris.append(sonucMatrisSatir)
		sonucMatrisSatir=[]

sonucMatrisSatirSayisi=len(sonucMatris)
sonucMatrisSutunSayisi=len(sonucMatris[0])

#sonuç matrisi ekrana yazdırma
print("\nİki matrisin çarpım sonucu: ")
satir=""
for h in range(sonucMatrisSatirSayisi):
	for g in range(sonucMatrisSutunSayisi):
		satir+=str(sonucMatris[h][g])+"\t"
	print(satir)
	satir=""
print("\n2 boyutlu dizi olarak sonuç:\n",sonucMatris)